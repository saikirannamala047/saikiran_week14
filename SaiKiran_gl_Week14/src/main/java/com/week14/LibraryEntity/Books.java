package com.week14.LibraryEntity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Books 
{
    @Id
    @GeneratedValue
    private long bookId;
    private String bookName;
    private String author;
    private float cost;

}
