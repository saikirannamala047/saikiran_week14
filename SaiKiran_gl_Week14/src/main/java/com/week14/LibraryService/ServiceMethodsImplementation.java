package com.week14.LibraryService;



import com.week14.LibraryRepository.Repo;

import com.week14.LibraryEntity.Books;
import com.week14.LibraryService.ServiceMethods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Sort;
import java.util.List;


//in this class, we are implementing the "ServiceMethods" interface to override all the methods declared in 
//that interface

@Service
public class ServiceMethodsImplementation implements ServiceMethods {


	@Autowired
    private Repo booksRepository;

    @Override
    public List<Books> getAllBooks() {
        return booksRepository.findAll();
    }

    @Override
    public Books getById(int bookId) {
        return booksRepository.findById(bookId).get();
    }

    @Override
    public List<Books> getByName(String bookName) {
        return booksRepository.findByBookName(bookName);
    }

    @Override
    public Books getByLastAdded() {
        List<Books> books=booksRepository.findAll();
        return books.get(books.size()-1);
    }

    @Override
    public List<Books> getByAuthor(String author) {
        return booksRepository.findByAuthor(author);
    }

    @Override
	public List<Books> getBooksByAscendingOrder() {
		return booksRepository.findAll(Sort.by("bookName"));
	}


    @Override
	public List<Books> addLibraryBooks(List<Books> books) {
		return booksRepository.saveAll(books);
	}


    @Override
    public void deleteById(int bookId) {
        booksRepository.deleteById(bookId);
    }

    @Override
    public void deleteAllBooks() {
        booksRepository.deleteAll();
    }

}
